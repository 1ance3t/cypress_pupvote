/// <reference types="Cypress" />
import MyAccount from '../../page_object/my_account.js'


context('E2E test My account page', () => {

    describe.only('Photo uploading', () => {

        beforeEach(() => {

            cy.login();

            MyAccount.openMyAccount();

            MyAccount.сloseModalFb();
        })

        it('Negative: Uploading image less than 270px png, jpeg, jpg, more then 5 mb, gif , broken image and txt', () => {

            MyAccount.uploadPhotoButton()
                .click();
    
            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/269.jpeg');
    
            MyAccount.emptyPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid')
    
            MyAccount.validationText()
                .should('be.visible')
    
            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/269.png');
    
            MyAccount.emptyPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid')
    
            MyAccount.validationText()
                .should('be.visible')
    
            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/800px-Snake_River_(5mb).jpg');
    
            MyAccount.emptyPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid')
    
            MyAccount.validationText()
                .should('be.visible')
    
            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/index.txt', { allowEmpty: true });
    
            MyAccount.emptyPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid')
    
            MyAccount.validationText()
                .should('be.visible')
    
            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/PallasJupiter.gif');
    
            MyAccount.emptyPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid')
    
            MyAccount.validationText()
                .should('be.visible')
    
            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/doge.jpg');
    
            MyAccount.emptyPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid')
    
            MyAccount.validationText()
                .should('be.visible')
        })


        it('Positive: Uploading correct jpeg image', () => {

            MyAccount.uploadPhotoButton()
                .click();

            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/275.jpeg');

            MyAccount.changeButton()
                .should('be.visible')

            MyAccount.imageCrop()
                .should('be.visible')

            MyAccount.applyPhotoBtn()
                .should('be.visible')
                .click()

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')

        })

        it('Positive: Uploading correct jpg image', () => {

            MyAccount.uploadPhotoButton()
                .click();

            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/275.jpg');

            MyAccount.changeButton()
                .should('be.visible')

            MyAccount.imageCrop()
                .should('be.visible')

            MyAccount.applyPhotoBtn()
                .should('be.visible')
                .click()

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')
        })

        it('Positive: Uploading correct png image', () => {

            MyAccount.uploadPhotoButton()
                .click();

            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/275.png');

            MyAccount.changeButton()
                .should('be.visible')

            MyAccount.imageCrop()
                .should('be.visible')

            MyAccount.applyPhotoBtn()
                .should('be.visible')
                .click()

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')

        })

    })

    describe('Name field', () => {

        beforeEach(() => {

            cy.login();

            MyAccount.openMyAccount();

            MyAccount.сloseModalFb();
        })

        it('Negative: Trying save empty nickname', () => {

            MyAccount.nameField()
                .clear()

            MyAccount.saveButton()
                .should('be.disabled')

        })

        it('Negative: Trying save nickname less then 2 symbols', () => {

            MyAccount.nameField().as('nameFiled')
                .clear()

            cy.fillField(1, '@nameFiled')

            MyAccount.saveButton()
                .should('be.disabled')

        })

        it('Negative: Trying save nickname with more then 60 symbols', () => {

            MyAccount.nameField().as('nameFiled')
                .clear()

            cy.fillField(61, '@nameFiled')

            MyAccount.nameSymbolCounter()
                .should('have.text', "61 / 60")

            MyAccount.saveButton()
                .should('be.disabled')

        })

        it('Positive: Trying save nickname with 2 symbols', () => {

            MyAccount.nameField().as('nameFiled')
                .clear()

            cy.fillField(2, '@nameFiled')

            MyAccount.nameSymbolCounter()
                .should('have.text', "2 / 60")

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')

            MyAccount.nameSymbolCounter()
                .should('have.text', "2 / 60")

            MyAccount.saveButton()
                .should('be.disabled')

        })

        it('Positive: Trying save nickname with 60 symbols', () => {

            MyAccount.nameField().as('nameFiled')
                .clear()

            cy.fillField(60, '@nameFiled')

            MyAccount.nameSymbolCounter()
                .should('have.text', "60 / 60")

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')

            MyAccount.nameSymbolCounter()
                .should('have.text', "60 / 60")

            MyAccount.saveButton()
                .should('be.disabled')

        })

    })

    describe('City field', () => {

        beforeEach(() => {

            cy.login();

            MyAccount.openMyAccount();

            MyAccount.сloseModalFb();

        })


        it('Positive: Trying save city with 1 symbols', () => {
            MyAccount.cityField().as('cityFiled')
                .clear()

            cy.fillField(1, '@cityFiled')

            MyAccount.citySymbolCounter()
                .should('have.text', "1 / 255")

            MyAccount.saveButton()
                .should('be.enabled')
                .click();

            MyAccount.citySymbolCounter()
                .should('have.text', "1 / 255")

            MyAccount.saveButton()
                .should('be.disabled')

        })


        it('Negative: Trying save city with more then 255 symbols', () => {

            MyAccount.cityField().as('cityFiled')
                .clear()

            cy.fillField(256, '@cityFiled')

            MyAccount.citySymbolCounter()
                .should('have.text', "256 / 255")

            MyAccount.saveButton()
                .should('be.disabled')

        })

        it('Positive: Trying save city with 255 symbols', () => {
            MyAccount.cityField().as('cityFiled')
                .clear()

            cy.fillField(255, '@cityFiled')

            MyAccount.citySymbolCounter()
                .should('have.text', "255 / 255")

            MyAccount.saveButton()
                .should('be.enabled')
                .click();

            MyAccount.citySymbolCounter()
                .should('have.text', "255 / 255")

            MyAccount.saveButton()
                .should('be.disabled')

        })

        it('Negative: Trying save empty city', () => {

            MyAccount.cityField()
                .click()
                .clear()

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar();

            MyAccount.cityField()
                .should('be.empty')

            MyAccount.saveButton()
                .should('be.disabled')

        })

    })

    describe('Checking the alet if dont save some field', () => {

        beforeEach(() => {

            cy.login();

            MyAccount.openMyAccount();

            MyAccount.сloseModalFb();

        })

        it('Checking the alet if dont save the city', () => {

            MyAccount.cityField()
                .type('1')

            cy.clickAllElementsInHeader();

            cy.clickAllElementsMyAccountDropdown();

            cy.clickItemsInSearch();

            cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
                .find('li').then((ul) => {
                    for (let i = 1; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        MyAccount.warningAlert()
                    }
                })
            cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
                .find('li').then((ul) => {
                    for (let i = 2; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        MyAccount.warningAlert()
                    }
                })

            cy.go('back')

            MyAccount.warningAlert()
        })

        it('Checking the alet if dont save the nickname', () => {

            MyAccount.nameField()
                .type('1')

            cy.clickAllElementsInHeader();

            cy.clickAllElementsMyAccountDropdown();

            cy.clickItemsInSearch();

            cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
                .find('li').then((ul) => {
                    for (let i = 1; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        MyAccount.warningAlert()
                    }
                })
            cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
                .find('li').then((ul) => {
                    for (let i = 2; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        MyAccount.warningAlert()
                    }
                })

            cy.go('back')

            MyAccount.warningAlert()
        })

        it('Checking the alet if dont save the photo', () => {

            MyAccount.uploadPhotoButton()
                .click();

            MyAccount.uploadPhotoField()
                .attachFile('/myAccountImages/275.jpg');

            MyAccount.changeButton()
                .should('be.visible')

            MyAccount.imageCrop()
                .should('be.visible')

            MyAccount.applyPhotoBtn()
                .should('be.visible')
                .click()

            MyAccount.saveButton()
                .should('be.enabled')

            cy.clickAllElementsInHeader();

            cy.clickAllElementsMyAccountDropdown();

            cy.clickItemsInSearch();

            cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
                .find('li').then((ul) => {
                    for (let i = 1; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        MyAccount.warningAlert()
                    }
                })
            cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
                .find('li').then((ul) => {
                    for (let i = 2; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        MyAccount.warningAlert()
                    }
                })

            cy.go('back')

            MyAccount.warningAlert()
        })

        it('Checking the alet if dont save the checkbox', () => {

            cy.get('.myaccount_checkBoxesWrapper__199Xe > :nth-child(1) > :nth-child(1) [type="checkbox"]').as('votesOnMyPost')
                .check()

            MyAccount.saveButton()
                .should('be.enabled')

            cy.clickAllElementsInHeader();

            cy.clickAllElementsMyAccountDropdown();

            cy.clickItemsInSearch();

            cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
                .find('li').then((ul) => {
                    for (let i = 1; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        MyAccount.warningAlert()
                    }
                })
            cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
                .find('li').then((ul) => {
                    for (let i = 2; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        MyAccount.warningAlert()
                    }
                })

            cy.go('back')

            MyAccount.warningAlert()
        })
    })

    describe('Email notifications', () => {

        beforeEach(() => {

            cy.login();

            MyAccount.openMyAccount();

            MyAccount.сloseModalFb();

        })

        it('Positive: Try to check all checkbox', () => {
            MyAccount.emailCheckboxes()
                .check()
                .should('be.checked')

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')

        })

        it('Positive: Try to uncheck all checkbox', () => {
            MyAccount.emailCheckboxes()
                .uncheck()
                .should('not.be.checked')

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')

        })

        it('Positive: Try to check all checkbox and click cancel button', () => {
            MyAccount.emailCheckboxes()
                .check()
                .should('be.checked')

            MyAccount.saveButton()
                .should('be.enabled')

            MyAccount.cancelButton()
                .should('be.enabled')
                .click()

            MyAccount.emailCheckboxes()
                .should('not.be.checked')

            MyAccount.saveButton()
                .should('be.disabled')

        })

        it('Positive: Try to ucheck all checkbox and click cancel button', () => {
            MyAccount.emailCheckboxes()
                .check()
                .should('be.checked')

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')

            MyAccount.emailCheckboxes()
                .should('be.checked')
                .uncheck()
                .should('not.be.checked')

            MyAccount.cancelButton()
                .should('be.enabled')
                .click()

            MyAccount.emailCheckboxes()
                .should('be.checked')

            MyAccount.saveButton()
                .should('be.disabled')

        })

        it('Positive: Try to uncheck and check one checkbox', () => {
            cy.get('.myaccount_checkBoxesWrapper__199Xe > :nth-child(1) > :nth-child(1) [type="checkbox"]').as('votesOnMyPost')
                .uncheck()

            cy.get('@votesOnMyPost')
                .should('be.not.be.checked')

            MyAccount.saveButton()
                .should('be.enabled')

            cy.get('@votesOnMyPost')
                .check()
                .should('be.checked')

            MyAccount.saveButton()
                .should('be.disabled')
        })

        it('Positive: Try to check and uncheck one checkbox', () => {
            cy.get('.myaccount_checkBoxesWrapper__199Xe > :nth-child(1) > :nth-child(1) [type="checkbox"]').as('votesOnMyPost')
                .uncheck()
                .should('be.not.be.checked')

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')

            cy.get('@votesOnMyPost')
                .should('be.not.be.checked')
                .check()
                .uncheck()
                .should('be.not.be.checked')

            MyAccount.saveButton()
                .should('be.disabled')

            MyAccount.emailCheckboxes()
                .uncheck()
                .should('be.not.be.checked')

            MyAccount.saveButton()
                .should('be.enabled')
                .click()

            MyAccount.succesSnackbar()
                .should('be.visible')
        })
    })

    describe('Cancel button', () => {

        beforeEach(() => {
    
            cy.login();
    
            MyAccount.openMyAccount();
    
            MyAccount.сloseModalFb();
    
        })
        it('Try type some text in the name field and click cancel button', () => {
            MyAccount.nameFieldValue()
                .invoke('val')
                .then(sometext => {
                    MyAccount.nameFieldValue()
                        .clear()
                        .type('a')
    
                    MyAccount.cancelButton()
                        .should('be.enabled')
                        .click()
    
                    MyAccount.nameFieldValue()
                        .should('have.value', sometext)
                })
        })
    
    
        it('Try type some character in the name field and click cancel button', () => {
            MyAccount.nameFieldValue()
                .invoke('val')
                .then(sometext => {
                    MyAccount.nameFieldValue()
                        .type('a')
                        .type('{backspace}')
                        .should('have.value', sometext)
    
                    MyAccount.saveButton()
                        .should('be.disabled')
    
                })
    
        })
    
        it('Try to clear field and type same text in the name field', () => {
            MyAccount.nameFieldValue()
                .invoke('val')
                .then(sometext => {
                    MyAccount.nameFieldValue()
                        .clear()
                        .type(sometext)
                        .should('have.value', sometext)
    
                    MyAccount.saveButton()
                        .should('be.disabled')
    
                })
    
        })
    
    
        it('Try type some text in the city field and click cancel button', () => {
            MyAccount.cityFieldValue()
                .type('city')
                .should('have.value', 'city')
    
            MyAccount.saveButton()
                .should('be.enabled')
                .click()
    
            MyAccount.succesSnackbar()
                .should('be.visible')
    
    
            MyAccount.cityFieldValue()
                .invoke('val')
                .then(sometext => {
                    MyAccount.cityFieldValue()
                        .clear()
                        .type('a')
    
                    MyAccount.cancelButton()
                        .should('be.enabled')
                        .click()
    
                    MyAccount.cityFieldValue()
                        .should('have.value', sometext)
    
                })
    
        })
    
        it('Try type some character in the city field and click cancel button, try to clear field and type same text in the city field', () => {
            MyAccount.cityFieldValue()
                .invoke('val')
                .then(sometext => {
                    cy.log(sometext)
                    MyAccount.cityFieldValue()
                        .type('a')
                        .type('{backspace}')
                        .should('have.value', sometext)
    
                    MyAccount.saveButton()
                        .should('be.disabled')
    
                })
    
            MyAccount.cityFieldValue()
                .invoke('val')
                .then(sometext => {
                    MyAccount.cityFieldValue()
                        .clear()
                        .type(sometext)
                        .should('have.value', sometext)
    
                    MyAccount.saveButton()
                        .should('be.disabled')
    
                })
    
            MyAccount.cityFieldValue()
                .clear()
    
            MyAccount.saveButton()
                .should('be.enabled')
                .click()
    
            MyAccount.succesSnackbar()
                .should('be.visible')
    
        })
    })

    describe('Delete account alert', () => {

        beforeEach(() => {

            cy.login();

            MyAccount.openMyAccount();

            MyAccount.сloseModalFb();

        })
        
        it('Positive: Checking the warning alerts if try delete account', () => {
            cy.get('.myaccount_actionsRow__3M3b_ > .small')
                .click()

            cy.get('.login-modal-container > .paragraph-2').as('warningText')
                .should('be.visible')

            cy.get('.login-modal-container > .text')
                .should('be.visible')
                .click()
            cy.get('@warningText')
                .should('be.visible')

            cy.get('.group-button > :nth-child(1)')
                .should('be.visible')
                .click()
        })
    })
        
})