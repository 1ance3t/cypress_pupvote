/// <reference types="Cypress" />
import AddPetPage from '../../page_object/add_pet.js'



context('E2E test Add pet apge', () => {

    describe('Photo uploading', () => {

        beforeEach(() => {
            cy.login()
    
            AddPetPage.openAddPetPage()
    
            AddPetPage.сloseModalFb()
    
        })
    
        it('Negative: Uploading image less than 640px png, jpeg, jpg, more then 5 mb, gif , broken image and txt', () => {
    
            AddPetPage.uploadPetPhotoButton()
                .click()
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('imagesAddPet/639.png');
    
            AddPetPage.emptyPetPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid add-pet-container')
    
            AddPetPage.validationText()
                .should('be.visible')
    
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('imagesAddPet/639.jpeg');
    
            AddPetPage.emptyPetPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid add-pet-container')
    
            AddPetPage.validationText()
                .should('be.visible')
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('imagesAddPet/639.jpeg');
    
            AddPetPage.emptyPetPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid add-pet-container')
    
            AddPetPage.validationText()
                .should('be.visible')
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('/myAccountImages/800px-Snake_River_(5mb).jpg');
    
            AddPetPage.emptyPetPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid add-pet-container')
    
            AddPetPage.validationText()
                .should('be.visible')
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('/myAccountImages/index.txt', { allowEmpty: true });
    
            AddPetPage.emptyPetPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid add-pet-container')
    
            AddPetPage.validationText()
                .should('be.visible')
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('/myAccountImages/PallasJupiter.gif');
    
            AddPetPage.emptyPetPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid add-pet-container')
    
            AddPetPage.validationText()
                .should('be.visible')
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('/myAccountImages/doge.jpg');
    
            AddPetPage.emptyPetPhotoPopup()
                .should('have.class', 'empty-photo-popup inValid add-pet-container')
    
            AddPetPage.validationText()
                .should('be.visible')
        })
    
        it('Positive: Uploading correct jpeg image', () => {
    
            AddPetPage.stateField()
                .click()
    
            cy.get('#downshift-0-item-1', { timeout: 30000 })
                .click()
    
            AddPetPage.petNameField()
                .as('petName')
                .clear()
    
            cy.fillField(5, '@petName')
    
            AddPetPage.petTypeSelect()
                .click();
    
            cy.get('[data-value="dog"]')
                .click()
    
            AddPetPage.contestPetBlock()
                .should('be.visible')
                .contains('Participate in the contest')
    
            AddPetPage.breedSelect()
                .click()
    
            cy.get('#downshift-1-item-1', { timeout: 30000 })
    
            AddPetPage.uploadPetPhotoButton()
                .click()
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('/imagesAddPet/640.jpeg');
    
            AddPetPage.changeButton()
                .should('be.visible')
    
            AddPetPage.imageCrop()
                .should('be.visible')
    
            AddPetPage.applyPhotoButton()
                .should('be.visible')
                .click()
    
            AddPetPage.contestPetBlock()
                .find('[type=checkbox]')
                .click()
    
            AddPetPage.saveButton()
                .should('be.enabled')
                .click()
    
            AddPetPage.succesSnackbar()
                .should('be.visible')
    
        })
    
        it('Positive: Uploading correct jpg image', () => {
    
            AddPetPage.stateField()
                .click()
    
            cy.get('#downshift-0-item-1', { timeout: 30000 })
                .click()
    
            AddPetPage.petNameField()
                .as('petName')
                .clear()
    
            cy.fillField(5, '@petName')
    
            AddPetPage.petTypeSelect()
                .click();
    
            cy.get('[data-value="dog"]')
                .click()
    
            AddPetPage.contestPetBlock()
                .should('be.visible')
                .contains('Participate in the contest')
    
            AddPetPage.breedSelect()
                .click()
    
            cy.get('#downshift-1-item-1', { timeout: 30000 })
    
            AddPetPage.uploadPetPhotoButton()
                .click()
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('/imagesAddPet/640.jpg');
    
            AddPetPage.changeButton()
                .should('be.visible')
    
            AddPetPage.imageCrop()
                .should('be.visible')
    
            AddPetPage.applyPhotoButton()
                .should('be.visible')
                .click()
    
            AddPetPage.contestPetBlock()
                .find('[type=checkbox]')
                .click()
    
            AddPetPage.saveButton()
                .should('be.enabled')
                .click()
    
            AddPetPage.succesSnackbar()
                .should('be.visible')
    
    
        })
    
        it('Positive: Uploading correct png image', () => {
    
            AddPetPage.stateField()
                .click()
    
            cy.get('#downshift-0-item-1', { timeout: 30000 })
                .click()
    
            AddPetPage.petNameField()
                .as('petName')
                .clear()
    
            cy.fillField(5, '@petName')
    
            AddPetPage.petTypeSelect()
                .click();
    
            cy.get('[data-value="dog"]')
                .click()
    
            AddPetPage.contestPetBlock()
                .should('be.visible')
                .contains('Participate in the contest')
    
            AddPetPage.breedSelect()
                .click()
    
            cy.get('#downshift-1-item-1', { timeout: 30000 })
    
            AddPetPage.uploadPetPhotoButton()
                .click()
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('/imagesAddPet/640.png');
    
            AddPetPage.changeButton()
                .should('be.visible')
    
            AddPetPage.imageCrop()
                .should('be.visible')
    
            AddPetPage.applyPhotoButton()
                .should('be.visible')
                .click()
    
            AddPetPage.contestPetBlock()
                .find('[type=checkbox]')
                .click()
    
            AddPetPage.saveButton()
                .should('be.enabled')
                .click()
    
            AddPetPage.succesSnackbar()
                .should('be.visible')
    
        })
    })

    describe('Pet name field', () => {

        beforeEach(() => {

            cy.login();

            AddPetPage.openAddPetPage();

            AddPetPage.сloseModalFb();
        })


        it('Negative: Trying save empty pet name, trying save less then 2 symbols', () => {

            AddPetPage.stateField()
                .click()
    
            cy.get('#downshift-0-item-1', { timeout: 30000 })
                .click()
    
            AddPetPage.petNameField()
                .as('petNameFiled')
                .clear()
    
            AddPetPage.petTypeSelect()
                .click();
    
            cy.get('[data-value="dog"]')
                .click()
    
            AddPetPage.contestPetBlock()
                .should('be.visible')
                .contains('Participate in the contest')
    
            AddPetPage.breedSelect()
                .click()
    
            cy.get('#downshift-1-item-1', { timeout: 30000 })
    
            AddPetPage.uploadPetPhotoButton()
                .click()
    
            AddPetPage.uploadPhotoFiled()
                .attachFile('/imagesAddPet/640.png');
    
            AddPetPage.changeButton()
                .should('be.visible')
    
            AddPetPage.imageCrop()
                .should('be.visible')
    
            AddPetPage.applyPhotoButton()
                .should('be.visible')
                .click()
    
            AddPetPage.contestPetBlock()
                .find('[type=checkbox]')
                .click()
    
            AddPetPage.saveButton()
                .should('be.disabled')
    
            AddPetPage.petNameField()
                .clear()
    
            cy.fillField(1, '@petNameFiled')
    
            AddPetPage.saveButton()
                .should('be.disabled')
    
        })

        it('Negative: Trying save pet name more then 60 symbols', () => {

            AddPetPage.stateField()
                .click()


            cy.get('#downshift-0-item-1', { timeout: 30000 })
                .click()

            AddPetPage.petNameField()
                .as('petNameFiled')
                .clear()


            cy.fillField(61, '@petNameFiled')
            AddPetPage.petNameFieldCounter()
                .should('have.text', '61 / 60')
                .should('have.class', 'chars input-error')

            AddPetPage.petTypeSelect()
                .click();

            cy.get('[data-value="dog"]')
                .click()

            AddPetPage.contestPetBlock()
                .should('be.visible')
                .contains('Participate in the contest')

            AddPetPage.breedSelect()
                .click()

            cy.get('#downshift-1-item-1', { timeout: 30000 })

            AddPetPage.uploadPetPhotoButton()
                .click()

            AddPetPage.uploadPhotoFiled()
                .attachFile('/imagesAddPet/640.png');

            AddPetPage.changeButton()
                .should('be.visible')

            AddPetPage.imageCrop()
                .should('be.visible')

            AddPetPage.applyPhotoButton()
                .should('be.visible')
                .click()

            AddPetPage.contestPetBlock()
                .find('[type=checkbox]')
                .click()

            AddPetPage.saveButton()
                .should('be.disabled')

        })

        it('Positive: Trying save pet name with 60 symbols', () => {

            AddPetPage.stateField()
                .click()


            cy.get('#downshift-0-item-1', { timeout: 30000 })
                .click()

            AddPetPage.petNameField()
                .as('petNameFiled')
                .clear()


            cy.fillField(60, '@petNameFiled')
            AddPetPage.petNameFieldCounter()
                .should('have.text', '60 / 60')
                .should('have.class', 'chars')

            AddPetPage.petTypeSelect()
                .click();

            cy.get('[data-value="dog"]')
                .click()

            AddPetPage.contestPetBlock()
                .should('be.visible')
                .contains('Participate in the contest')

            AddPetPage.breedSelect()
                .click()

            cy.get('#downshift-1-item-1', { timeout: 30000 })

            AddPetPage.uploadPetPhotoButton()
                .click()

            AddPetPage.uploadPhotoFiled()
                .attachFile('/imagesAddPet/640.png');

            AddPetPage.changeButton()
                .should('be.visible')

            AddPetPage.imageCrop()
                .should('be.visible')

            AddPetPage.applyPhotoButton()
                .should('be.visible')
                .click()

            AddPetPage.contestPetBlock()
                .find('[type=checkbox]')
                .click()

            AddPetPage.saveButton()
                .click()

            AddPetPage.succesSnackbar()
                .should('be.visible')

        })

        it('Positive: Trying save pet name with 2 symbols', () => {

            AddPetPage.stateField()
                .click()


            cy.get('#downshift-0-item-1', { timeout: 30000 })
                .click()

            AddPetPage.petNameField()
                .as('petNameFiled')
                .clear()


            cy.fillField(2, '@petNameFiled')
            AddPetPage.petNameFieldCounter()
                .should('have.text', '2 / 60')
                .should('have.class', 'chars')

            AddPetPage.petTypeSelect()
                .click();

            cy.get('[data-value="dog"]')
                .click()

            AddPetPage.contestPetBlock()
                .should('be.visible')
                .contains('Participate in the contest')

            AddPetPage.breedSelect()
                .click()

            cy.get('#downshift-1-item-1', { timeout: 30000 })

            AddPetPage.uploadPetPhotoButton()
                .click()

            AddPetPage.uploadPhotoFiled()
                .attachFile('/imagesAddPet/640.png');

            AddPetPage.changeButton()
                .should('be.visible')

            AddPetPage.imageCrop()
                .should('be.visible')

            AddPetPage.applyPhotoButton()
                .should('be.visible')
                .click()

            AddPetPage.contestPetBlock()
                .find('[type=checkbox]')
                .click()

            AddPetPage.saveButton()
                .click()

            AddPetPage.succesSnackbar()
                .should('be.visible')

        })
    })

    // describe('Breed field', () => {

    //     beforeEach(() => {

    //         cy.login();

    //         AddPetPage.openAddPetPage();

    //         AddPetPage.сloseModalFb();
    //     })

    //     it('Positive: Try to select all dog breeds from list', () => {

    //         AddPetPage.petTypeSelect()
    //             .click();

    //         cy.get('[data-value="dog"]')
    //             .click()

    //         cy.get('.jss291', { timeout: 30000 })
    //             .should('be.visible').contains('Participate in the contest')

    //         AddPetPage.breedSelect()
    //             .click()

    //         AddPetPage.listOfItems()
    //             .find('li').then((li) => {
    //                 for (let i = 0; i < li.length; i++) {
    //                     cy.get(`#downshift-1-item-${i}`, { timeout: 30000 })
    //                         .click()

    //                     cy.get('[id="select-breed"]')
    //                         .should('have.text', li[i].outerText)

    //                     cy.get('#downshift-1-input')
    //                         .click()
    //                         .clear()
    //                 }
    //             })
    //     })

    //     it('Positive: Try to select all cat breeds from list', () => {

    //         AddPetPage.petTypeSelect()
    //             .click();

    //         cy.get('[data-value="cat"]')
    //             .click()

    //         cy.get('.jss291', { timeout: 30000 })
    //             .should('be.visible').contains('Participate in the contest')

    //         AddPetPage.breedSelect()
    //             .click()

    //         AddPetPage.listOfItems()
    //             .find('li').then((li) => {
    //                 for (let i = 0; i < li.length; i++) {
    //                     cy.get(`#downshift-1-item-${i}`, { timeout: 30000 })
    //                         .click()

    //                     cy.get('[id="select-breed"]')
    //                         .should('have.text', li[i].outerText)

    //                     cy.get('#downshift-1-input')
    //                         .click()
    //                         .clear()
    //                 }
    //             })
    //     })


    // })

    describe('Checking the alet if dont save some field', () => {

        beforeEach(() => {

            cy.login();

            AddPetPage.openAddPetPage();

            AddPetPage.сloseModalFb();

        })

        it('Checking the alet if dont save the pet name', () => {

            AddPetPage.petNameField()
                .type('1')

            cy.get('.header_desktopContainer__2ghst [type="button"]')
                .as('header')
                .then((link) => {
                    for (let i = 0; i < link.length; i++) {
                        if (link[i].innerText != "Add Pet") {
                            cy.get(link[i])
                                .click()
                            AddPetPage.warningAlert()
                        }
                    }
                })

            cy.clickItemsInSearch();

            cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
                .find('li').then((ul) => {
                    for (let i = 1; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })
            cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
                .find('li').then((ul) => {
                    for (let i = 2; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })

            cy.go('back')

            AddPetPage.warningAlert()
        })

        it('Checking the alet if dont save the region', () => {

            AddPetPage.stateField()
                .click()


            cy.get('#downshift-0-item-1', { timeout: 30000 })
                .click()

            cy.get('.header_desktopContainer__2ghst [type="button"]')
                .as('header')
                .then((link) => {
                    for (let i = 0; i < link.length; i++) {
                        if (link[i].innerText != "Add Pet") {
                            cy.get(link[i])
                                .click()
                            AddPetPage.warningAlert()
                        }
                    }
                })

            cy.clickItemsInSearch();

            cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
                .find('li').then((ul) => {
                    for (let i = 1; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })
            cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
                .find('li').then((ul) => {
                    for (let i = 2; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })

            cy.go('back')

            AddPetPage.warningAlert()
        })

        it('Checking the alet if dont save the breed and type', () => {

            AddPetPage.petTypeSelect()
                .click();

            cy.get('[data-value="dog"]')
                .click()

            AddPetPage.contestPetBlock()
                .should('be.visible')
                .contains('Participate in the contest')

            AddPetPage.breedSelect()
                .click()

            cy.get('#downshift-1-item-1', { timeout: 30000 })

            cy.get('.header_desktopContainer__2ghst [type="button"]')
                .as('header')
                .then((link) => {
                    for (let i = 0; i < link.length; i++) {
                        if (link[i].innerText != "Add Pet") {
                            cy.get(link[i])
                                .click()
                            AddPetPage.warningAlert()
                        }
                    }
                })

            cy.clickItemsInSearch();

            cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
                .find('li').then((ul) => {
                    for (let i = 1; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })
            cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
                .find('li').then((ul) => {
                    for (let i = 2; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })

            cy.go('back')

            AddPetPage.warningAlert()
        })

        it('Checking the alet if dont save bio', () => {

            AddPetPage.bioField()
                .click()
                .type('1')

            cy.get('.header_desktopContainer__2ghst [type="button"]')
                .as('header')
                .then((link) => {
                    for (let i = 0; i < link.length; i++) {
                        if (link[i].innerText != "Add Pet") {
                            cy.get(link[i])
                                .click()
                            AddPetPage.warningAlert()
                        }
                    }
                })

            cy.clickItemsInSearch();

            cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
                .find('li').then((ul) => {
                    for (let i = 1; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })
            cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
                .find('li').then((ul) => {
                    for (let i = 2; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })

            cy.go('back')

            AddPetPage.warningAlert()
        })


        it('Checking the alet if dont save photo', () => {

            AddPetPage.uploadPetPhotoButton()
                .click()

            AddPetPage.uploadPhotoFiled()
                .attachFile('/imagesAddPet/640.png');

            AddPetPage.changeButton()
                .should('be.visible')

            AddPetPage.imageCrop()
                .should('be.visible')

            AddPetPage.applyPhotoButton()
                .should('be.visible')
                .click()

            cy.get('.header_desktopContainer__2ghst [type="button"]')
                .as('header')
                .then((link) => {
                    for (let i = 0; i < link.length; i++) {
                        if (link[i].innerText != "Add Pet") {
                            cy.get(link[i])
                                .click()
                            AddPetPage.warningAlert()
                        }
                    }
                })

            cy.clickItemsInSearch();

            cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
                .find('li').then((ul) => {
                    for (let i = 1; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })
            cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
                .find('li').then((ul) => {
                    for (let i = 2; i < ul.length; i++) {
                        cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                            .click()
                        AddPetPage.warningAlert()
                    }
                })

            cy.go('back')

            AddPetPage.warningAlert()
        })
    })
})