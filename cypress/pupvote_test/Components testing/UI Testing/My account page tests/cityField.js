/// <reference types="Cypress" />
import MyAccount from '../../../../page_object/my_account.js'

describe('City field', () => {

    beforeEach(() => {

        cy.login();

        MyAccount.openMyAccount();

        MyAccount.сloseModalFb();

    })


    it('Positive: Trying save city with 1 symbols', () => {
        MyAccount.cityField().as('cityFiled')
            .clear()

        cy.fillField(1, '@cityFiled')

        MyAccount.citySymbolCounter()
            .should('have.text', "1 / 255")

        MyAccount.saveButton()
            .should('be.enabled')
            .click();

        MyAccount.citySymbolCounter()
            .should('have.text', "1 / 255")

        MyAccount.saveButton()
            .should('be.disabled')

    })


    it('Negative: Trying save city with more then 255 symbols', () => {

        MyAccount.cityField().as('cityFiled')
            .clear()

        cy.fillField(256, '@cityFiled')

        MyAccount.citySymbolCounter()
            .should('have.text', "256 / 255")

        MyAccount.saveButton()
            .should('be.disabled')

    })

    it('Positive: Trying save city with 255 symbols', () => {
        MyAccount.cityField().as('cityFiled')
            .clear()

        cy.fillField(255, '@cityFiled')

        MyAccount.citySymbolCounter()
            .should('have.text', "255 / 255")

        MyAccount.saveButton()
            .should('be.enabled')
            .click();

        MyAccount.citySymbolCounter()
            .should('have.text', "255 / 255")

        MyAccount.saveButton()
            .should('be.disabled')

    })

    it('Negative: Trying save empty city', () => {

        MyAccount.cityField()
            .click()
            .clear()

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar();

        MyAccount.cityField()
            .should('be.empty')

        MyAccount.saveButton()
            .should('be.disabled')

    })

})