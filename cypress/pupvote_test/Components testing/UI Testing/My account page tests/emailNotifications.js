/// <reference types="Cypress" />
import MyAccount from '../../../../page_object/my_account.js'


describe('Email notifications', () => {

    beforeEach(() => {

        cy.login();

        MyAccount.openMyAccount();

        MyAccount.сloseModalFb();

    })

    it('Positive: Try to check all checkbox', () => {
        MyAccount.emailCheckboxes()
            .check()
            .should('be.checked')

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')

    })

    it('Positive: Try to uncheck all checkbox', () => {
        MyAccount.emailCheckboxes()
            .uncheck()
            .should('not.be.checked')

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')

    })

    it('Positive: Try to check all checkbox and click cancel button', () => {
        MyAccount.emailCheckboxes()
            .check()
            .should('be.checked')

        MyAccount.saveButton()
            .should('be.enabled')

        MyAccount.cancelButton()
            .should('be.enabled')
            .click()

        MyAccount.emailCheckboxes()
            .should('not.be.checked')

        MyAccount.saveButton()
            .should('be.disabled')

    })

    it('Positive: Try to ucheck all checkbox and click cancel button', () => {
        MyAccount.emailCheckboxes()
            .check()
            .should('be.checked')

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')

        MyAccount.emailCheckboxes()
            .should('be.checked')
            .uncheck()
            .should('not.be.checked')

        MyAccount.cancelButton()
            .should('be.enabled')
            .click()

        MyAccount.emailCheckboxes()
            .should('be.checked')

        MyAccount.saveButton()
            .should('be.disabled')

    })

    it('Positive: Try to uncheck and check one checkbox', () => {
        cy.get('.myaccount_checkBoxesWrapper__199Xe > :nth-child(1) > :nth-child(1) [type="checkbox"]').as('votesOnMyPost')
            .uncheck()

        cy.get('@votesOnMyPost')
            .should('be.not.be.checked')

        MyAccount.saveButton()
            .should('be.enabled')

        cy.get('@votesOnMyPost')
            .check()
            .should('be.checked')

        MyAccount.saveButton()
            .should('be.disabled')
    })

    it('Positive: Try to check and uncheck one checkbox', () => {
        cy.get('.myaccount_checkBoxesWrapper__199Xe > :nth-child(1) > :nth-child(1) [type="checkbox"]').as('votesOnMyPost')
            .uncheck()
            .should('be.not.be.checked')

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')

        cy.get('@votesOnMyPost')
            .should('be.not.be.checked')
            .check()
            .uncheck()
            .should('be.not.be.checked')

        MyAccount.saveButton()
            .should('be.disabled')

        MyAccount.emailCheckboxes()
            .uncheck()
            .should('be.not.be.checked')

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')
    })
})