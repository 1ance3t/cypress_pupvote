/// <reference types="Cypress" />
import MyAccount from '../../../../page_object/my_account.js'

describe('Checking the alet if dont save some field', () => {

    beforeEach(() => {

        cy.login();

        MyAccount.openMyAccount();

        MyAccount.сloseModalFb();

    })

    it('Checking the alet if dont save the city', () => {

        MyAccount.cityField()
            .type('1')

        cy.clickAllElementsInHeader();

        cy.clickAllElementsMyAccountDropdown();

        cy.clickItemsInSearch();

        cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
            .find('li').then((ul) => {
                for (let i = 1; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    MyAccount.warningAlert()
                }
            })
        cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
            .find('li').then((ul) => {
                for (let i = 2; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    MyAccount.warningAlert()
                }
            })

        cy.go('back')

        MyAccount.warningAlert()
    })

    it('Checking the alet if dont save the nickname', () => {

        MyAccount.nameField()
            .type('1')

        cy.clickAllElementsInHeader();

        cy.clickAllElementsMyAccountDropdown();

        cy.clickItemsInSearch();

        cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
            .find('li').then((ul) => {
                for (let i = 1; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    MyAccount.warningAlert()
                }
            })
        cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
            .find('li').then((ul) => {
                for (let i = 2; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    MyAccount.warningAlert()
                }
            })

        cy.go('back')

        MyAccount.warningAlert()
    })

    it('Checking the alet if dont save the photo', () => {

        MyAccount.uploadPhotoButton()
            .click();

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/275.jpg');

        MyAccount.changeButton()
            .should('be.visible')

        MyAccount.imageCrop()
            .should('be.visible')

        MyAccount.applyPhotoBtn()
            .should('be.visible')
            .click()

        MyAccount.saveButton()
            .should('be.enabled')

        cy.clickAllElementsInHeader();

        cy.clickAllElementsMyAccountDropdown();

        cy.clickItemsInSearch();

        cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
            .find('li').then((ul) => {
                for (let i = 1; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    MyAccount.warningAlert()
                }
            })
        cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
            .find('li').then((ul) => {
                for (let i = 2; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    MyAccount.warningAlert()
                }
            })

        cy.go('back')

        MyAccount.warningAlert()
    })

    it('Checking the alet if dont save the checkbox', () => {

        cy.get('.myaccount_checkBoxesWrapper__199Xe > :nth-child(1) > :nth-child(1) [type="checkbox"]').as('votesOnMyPost')
            .check()

        MyAccount.saveButton()
            .should('be.enabled')

        cy.clickAllElementsInHeader();

        cy.clickAllElementsMyAccountDropdown();

        cy.clickItemsInSearch();

        cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
            .find('li').then((ul) => {
                for (let i = 1; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    MyAccount.warningAlert()
                }
            })
        cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
            .find('li').then((ul) => {
                for (let i = 2; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    MyAccount.warningAlert()
                }
            })

        cy.go('back')

        MyAccount.warningAlert()
    })
})