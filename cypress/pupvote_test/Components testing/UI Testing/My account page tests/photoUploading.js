/// <reference types="Cypress" />
import MyAccount from '../../../../page_object/my_account'

describe('Photo uploading', () => {

    beforeEach(() => {

        cy.login();

        MyAccount.openMyAccount();

        MyAccount.сloseModalFb();
    })

    it('Negative: Uploading image less than 270px png, jpeg, jpg, more then 5 mb, gif , broken image and txt', () => {

        MyAccount.uploadPhotoButton()
            .click();

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/269.jpeg');

        MyAccount.emptyPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid')

        MyAccount.validationText()
            .should('be.visible')

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/269.png');

        MyAccount.emptyPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid')

        MyAccount.validationText()
            .should('be.visible')

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/800px-Snake_River_(5mb).jpg');

        MyAccount.emptyPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid')

        MyAccount.validationText()
            .should('be.visible')

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/index.txt', { allowEmpty: true });

        MyAccount.emptyPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid')

        MyAccount.validationText()
            .should('be.visible')

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/PallasJupiter.gif');

        MyAccount.emptyPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid')

        MyAccount.validationText()
            .should('be.visible')

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/doge.jpg');

        MyAccount.emptyPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid')

        MyAccount.validationText()
            .should('be.visible')
    })


    it('Positive: Uploading correct jpeg image', () => {

        MyAccount.uploadPhotoButton()
            .click();

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/275.jpeg');

        MyAccount.changeButton()
            .should('be.visible')

        MyAccount.imageCrop()
            .should('be.visible')

        MyAccount.applyPhotoBtn()
            .should('be.visible')
            .click()

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')

    })

    it('Positive: Uploading correct jpg image', () => {

        MyAccount.uploadPhotoButton()
            .click();

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/275.jpg');

        MyAccount.changeButton()
            .should('be.visible')

        MyAccount.imageCrop()
            .should('be.visible')

        MyAccount.applyPhotoBtn()
            .should('be.visible')
            .click()

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')
    })

    it('Positive: Uploading correct png image', () => {

        MyAccount.uploadPhotoButton()
            .click();

        MyAccount.uploadPhotoField()
            .attachFile('/myAccountImages/275.png');

        MyAccount.changeButton()
            .should('be.visible')

        MyAccount.imageCrop()
            .should('be.visible')

        MyAccount.applyPhotoBtn()
            .should('be.visible')
            .click()

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')

    })

})