/// <reference types="Cypress" />
import MyAccount from '../../../../page_object/my_account.js'

describe('Name field', () => {

    beforeEach(() => {

        cy.login();

        MyAccount.openMyAccount();

        MyAccount.сloseModalFb();
    })

    it('Negative: Trying save empty nickname', () => {

        MyAccount.nameField()
            .clear()

        MyAccount.saveButton()
            .should('be.disabled')

    })

    it('Negative: Trying save nickname less then 2 symbols', () => {

        MyAccount.nameField().as('nameFiled')
            .clear()

        cy.fillField(1, '@nameFiled')

        MyAccount.saveButton()
            .should('be.disabled')

    })

    it('Negative: Trying save nickname with more then 60 symbols', () => {

        MyAccount.nameField().as('nameFiled')
            .clear()

        cy.fillField(61, '@nameFiled')

        MyAccount.nameSymbolCounter()
            .should('have.text', "61 / 60")

        MyAccount.saveButton()
            .should('be.disabled')

    })

    it('Positive: Trying save nickname with 2 symbols', () => {

        MyAccount.nameField().as('nameFiled')
            .clear()

        cy.fillField(2, '@nameFiled')

        MyAccount.nameSymbolCounter()
            .should('have.text', "2 / 60")

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')

        MyAccount.nameSymbolCounter()
            .should('have.text', "2 / 60")

        MyAccount.saveButton()
            .should('be.disabled')

    })

    it('Positive: Trying save nickname with 60 symbols', () => {

        MyAccount.nameField().as('nameFiled')
            .clear()

        cy.fillField(60, '@nameFiled')

        MyAccount.nameSymbolCounter()
            .should('have.text', "60 / 60")

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')

        MyAccount.nameSymbolCounter()
            .should('have.text', "60 / 60")

        MyAccount.saveButton()
            .should('be.disabled')

    })

})