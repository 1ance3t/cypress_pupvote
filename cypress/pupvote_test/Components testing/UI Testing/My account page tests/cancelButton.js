/// <reference types="Cypress" />
import MyAccount from '../../../../page_object/my_account.js'

describe('Cancel button', () => {

    beforeEach(() => {

        cy.login();

        MyAccount.openMyAccount();

        MyAccount.сloseModalFb();

    })
    it('Try type some text in the name field and click cancel button', () => {
        MyAccount.nameFieldValue()
            .invoke('val')
            .then(sometext => {
                MyAccount.nameFieldValue()
                    .clear()
                    .type('a')

                MyAccount.cancelButton()
                    .should('be.enabled')
                    .click()

                MyAccount.nameFieldValue()
                    .should('have.value', sometext)
            })
    })


    it('Try type some character in the name field and click cancel button', () => {
        MyAccount.nameFieldValue()
            .invoke('val')
            .then(sometext => {
                MyAccount.nameFieldValue()
                    .type('a')
                    .type('{backspace}')
                    .should('have.value', sometext)

                MyAccount.saveButton()
                    .should('be.disabled')

            })

    })

    it('Try to clear field and type same text in the name field', () => {
        MyAccount.nameFieldValue()
            .invoke('val')
            .then(sometext => {
                MyAccount.nameFieldValue()
                    .clear()
                    .type(sometext)
                    .should('have.value', sometext)

                MyAccount.saveButton()
                    .should('be.disabled')

            })

    })


    it('Try type some text in the city field and click cancel button', () => {
        MyAccount.cityFieldValue()
            .type('city')
            .should('have.value', 'city')

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')


        MyAccount.cityFieldValue()
            .invoke('val')
            .then(sometext => {
                MyAccount.cityFieldValue()
                    .clear()
                    .type('a')

                MyAccount.cancelButton()
                    .should('be.enabled')
                    .click()

                MyAccount.cityFieldValue()
                    .should('have.value', sometext)

            })

    })

    it('Try type some character in the city field and click cancel button, try to clear field and type same text in the city field', () => {
        MyAccount.cityFieldValue()
            .invoke('val')
            .then(sometext => {
                cy.log(sometext)
                MyAccount.cityFieldValue()
                    .type('a')
                    .type('{backspace}')
                    .should('have.value', sometext)

                MyAccount.saveButton()
                    .should('be.disabled')

            })

        MyAccount.cityFieldValue()
            .invoke('val')
            .then(sometext => {
                MyAccount.cityFieldValue()
                    .clear()
                    .type(sometext)
                    .should('have.value', sometext)

                MyAccount.saveButton()
                    .should('be.disabled')

            })

        MyAccount.cityFieldValue()
            .clear()

        MyAccount.saveButton()
            .should('be.enabled')
            .click()

        MyAccount.succesSnackbar()
            .should('be.visible')

    })
})