/// <reference types="Cypress" />
import MyAccount from '../../../../page_object/my_account.js'


describe('Delete account alert', () => {

    beforeEach(() => {

        cy.login();

        MyAccount.openMyAccount();

        MyAccount.сloseModalFb();

    })
    
    it('Positive: Checking the warning alerts if try delete account', () => {
        cy.get('.myaccount_actionsRow__3M3b_ > .small')
            .click()

        cy.get('.login-modal-container > .paragraph-2').as('warningText')
            .should('be.visible')

        cy.get('.login-modal-container > .text')
            .should('be.visible')
            .click()
        cy.get('@warningText')
            .should('be.visible')

        cy.get('.group-button > :nth-child(1)')
            .should('be.visible')
            .click()
    })
})