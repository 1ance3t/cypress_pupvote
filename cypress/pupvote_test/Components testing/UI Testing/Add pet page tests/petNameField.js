/// <reference types="Cypress" />
import AddPetPage from '../../../../page_object/add_pet.js'

describe('Pet name field', () => {

    beforeEach(() => {

        cy.login();

        AddPetPage.openAddPetPage();

        AddPetPage.сloseModalFb();
    })


    it('Negative: Trying save empty pet name, trying save less then 2 symbols', () => {

        AddPetPage.stateField()
            .click()

        cy.get('#downshift-0-item-1', { timeout: 30000 })
            .click()

        AddPetPage.petNameField()
            .as('petNameFiled')
            .clear()

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="dog"]')
            .click()

        AddPetPage.contestPetBlock()
            .should('be.visible')
            .contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        cy.get('#downshift-1-item-1', { timeout: 30000 })

        AddPetPage.uploadPetPhotoButton()
            .click()

        AddPetPage.uploadPhotoFiled()
            .attachFile('/imagesAddPet/640.png');

        AddPetPage.changeButton()
            .should('be.visible')

        AddPetPage.imageCrop()
            .should('be.visible')

        AddPetPage.applyPhotoButton()
            .should('be.visible')
            .click()

        AddPetPage.contestPetBlock()
            .find('[type=checkbox]')
            .click()

        AddPetPage.saveButton()
            .should('be.disabled')

        AddPetPage.petNameField()
            .clear()

        cy.fillField(1, '@petNameFiled')

        AddPetPage.saveButton()
            .should('be.disabled')

    })

    it('Negative: Trying save pet name more then 60 symbols', () => {

        AddPetPage.stateField()
            .click()


        cy.get('#downshift-0-item-1', { timeout: 30000 })
            .click()

        AddPetPage.petNameField()
            .as('petNameFiled')
            .clear()


        cy.fillField(61, '@petNameFiled')
        AddPetPage.petNameFieldCounter()
            .should('have.text', '61 / 60')
            .should('have.class', 'chars input-error')

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="dog"]')
            .click()

        AddPetPage.contestPetBlock()
            .should('be.visible')
            .contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        cy.get('#downshift-1-item-1', { timeout: 30000 })

        AddPetPage.uploadPetPhotoButton()
            .click()

        AddPetPage.uploadPhotoFiled()
            .attachFile('/imagesAddPet/640.png');

        AddPetPage.changeButton()
            .should('be.visible')

        AddPetPage.imageCrop()
            .should('be.visible')

        AddPetPage.applyPhotoButton()
            .should('be.visible')
            .click()

        AddPetPage.contestPetBlock()
            .find('[type=checkbox]')
            .click()

        AddPetPage.saveButton()
            .should('be.disabled')

    })

    it('Positive: Trying save pet name with 60 symbols', () => {

        AddPetPage.stateField()
            .click()


        cy.get('#downshift-0-item-1', { timeout: 30000 })
            .click()

        AddPetPage.petNameField()
            .as('petNameFiled')
            .clear()


        cy.fillField(60, '@petNameFiled')
        AddPetPage.petNameFieldCounter()
            .should('have.text', '60 / 60')
            .should('have.class', 'chars')

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="dog"]')
            .click()

        AddPetPage.contestPetBlock()
            .should('be.visible')
            .contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        cy.get('#downshift-1-item-1', { timeout: 30000 })

        AddPetPage.uploadPetPhotoButton()
            .click()

        AddPetPage.uploadPhotoFiled()
            .attachFile('/imagesAddPet/640.png');

        AddPetPage.changeButton()
            .should('be.visible')

        AddPetPage.imageCrop()
            .should('be.visible')

        AddPetPage.applyPhotoButton()
            .should('be.visible')
            .click()

        AddPetPage.contestPetBlock()
            .find('[type=checkbox]')
            .click()

        AddPetPage.saveButton()
            .click()

        AddPetPage.succesSnackbar()
            .should('be.visible')

    })

    it('Positive: Trying save pet name with 2 symbols', () => {

        AddPetPage.stateField()
            .click()


        cy.get('#downshift-0-item-1', { timeout: 30000 })
            .click()

        AddPetPage.petNameField()
            .as('petNameFiled')
            .clear()


        cy.fillField(2, '@petNameFiled')
        AddPetPage.petNameFieldCounter()
            .should('have.text', '2 / 60')
            .should('have.class', 'chars')

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="dog"]')
            .click()

        AddPetPage.contestPetBlock()
            .should('be.visible')
            .contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        cy.get('#downshift-1-item-1', { timeout: 30000 })

        AddPetPage.uploadPetPhotoButton()
            .click()

        AddPetPage.uploadPhotoFiled()
            .attachFile('/imagesAddPet/640.png');

        AddPetPage.changeButton()
            .should('be.visible')

        AddPetPage.imageCrop()
            .should('be.visible')

        AddPetPage.applyPhotoButton()
            .should('be.visible')
            .click()

        AddPetPage.contestPetBlock()
            .find('[type=checkbox]')
            .click()

        AddPetPage.saveButton()
            .click()

        AddPetPage.succesSnackbar()
            .should('be.visible')

    })
})