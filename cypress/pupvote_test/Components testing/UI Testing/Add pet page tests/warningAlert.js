/// <reference types="Cypress" />
import AddPetPage from '../../../../page_object/add_pet.js'


describe('Checking the alet if dont save some field', () => {

    beforeEach(() => {

        cy.login();

        AddPetPage.openAddPetPage();

        AddPetPage.сloseModalFb();

    })

    it('Checking the alet if dont save the pet name', () => {

        AddPetPage.petNameField()
            .type('1')

        cy.get('.header_desktopContainer__2ghst [type="button"]')
            .as('header')
            .then((link) => {
                for (let i = 0; i < link.length; i++) {
                    if (link[i].innerText != "Add Pet") {
                        cy.get(link[i])
                            .click()
                        AddPetPage.warningAlert()
                    }
                }
            })

        cy.clickItemsInSearch();

        cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
            .find('li').then((ul) => {
                for (let i = 1; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })
        cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
            .find('li').then((ul) => {
                for (let i = 2; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })

        cy.go('back')

        AddPetPage.warningAlert()
    })

    it('Checking the alet if dont save the region', () => {

        AddPetPage.stateField()
            .click()


        cy.get('#downshift-0-item-1', { timeout: 30000 })
            .click()

        cy.get('.header_desktopContainer__2ghst [type="button"]')
            .as('header')
            .then((link) => {
                for (let i = 0; i < link.length; i++) {
                    if (link[i].innerText != "Add Pet") {
                        cy.get(link[i])
                            .click()
                        AddPetPage.warningAlert()
                    }
                }
            })

        cy.clickItemsInSearch();

        cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
            .find('li').then((ul) => {
                for (let i = 1; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })
        cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
            .find('li').then((ul) => {
                for (let i = 2; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })

        cy.go('back')

        AddPetPage.warningAlert()
    })

    it('Checking the alet if dont save the breed and type', () => {

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="dog"]')
            .click()

        AddPetPage.contestPetBlock()
            .should('be.visible')
            .contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        cy.get('#downshift-1-item-1', { timeout: 30000 })

        cy.get('.header_desktopContainer__2ghst [type="button"]')
            .as('header')
            .then((link) => {
                for (let i = 0; i < link.length; i++) {
                    if (link[i].innerText != "Add Pet") {
                        cy.get(link[i])
                            .click()
                        AddPetPage.warningAlert()
                    }
                }
            })

        cy.clickItemsInSearch();

        cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
            .find('li').then((ul) => {
                for (let i = 1; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })
        cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
            .find('li').then((ul) => {
                for (let i = 2; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })

        cy.go('back')

        AddPetPage.warningAlert()
    })

    it('Checking the alet if dont save bio', () => {

        AddPetPage.bioField()
            .click()
            .type('1')

        cy.get('.header_desktopContainer__2ghst [type="button"]')
            .as('header')
            .then((link) => {
                for (let i = 0; i < link.length; i++) {
                    if (link[i].innerText != "Add Pet") {
                        cy.get(link[i])
                            .click()
                        AddPetPage.warningAlert()
                    }
                }
            })

        cy.clickItemsInSearch();

        cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
            .find('li').then((ul) => {
                for (let i = 1; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })
        cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
            .find('li').then((ul) => {
                for (let i = 2; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })

        cy.go('back')

        AddPetPage.warningAlert()
    })


    it('Checking the alet if dont save photo', () => {

        AddPetPage.uploadPetPhotoButton()
            .click()

        AddPetPage.uploadPhotoFiled()
            .attachFile('/imagesAddPet/640.png');

        AddPetPage.changeButton()
            .should('be.visible')

        AddPetPage.imageCrop()
            .should('be.visible')

        AddPetPage.applyPhotoButton()
            .should('be.visible')
            .click()

        cy.get('.header_desktopContainer__2ghst [type="button"]')
            .as('header')
            .then((link) => {
                for (let i = 0; i < link.length; i++) {
                    if (link[i].innerText != "Add Pet") {
                        cy.get(link[i])
                            .click()
                        AddPetPage.warningAlert()
                    }
                }
            })

        cy.clickItemsInSearch();

        cy.get('#root > div > footer > div > div.footer_topFooter__306-x > ul')
            .find('li').then((ul) => {
                for (let i = 1; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_topFooter__306-x > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })
        cy.get('#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul')
            .find('li').then((ul) => {
                for (let i = 2; i < ul.length; i++) {
                    cy.get(`#root > div > footer > div > div.footer_middleFooter__32zFl > div:nth-child(2) > ul > li:nth-child(${i})`, { timeout: 30000 })
                        .click()
                    AddPetPage.warningAlert()
                }
            })

        cy.go('back')

        AddPetPage.warningAlert()
    })
})