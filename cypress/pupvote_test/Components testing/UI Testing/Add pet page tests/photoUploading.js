/// <reference types="Cypress" />
import AddPetPage from '../../../../page_object/add_pet.js'


describe('Photo uploading', () => {

    beforeEach(() => {
        cy.login()

        AddPetPage.openAddPetPage()

        AddPetPage.сloseModalFb()

    })

    it('Negative: Uploading image less than 640px png, jpeg, jpg, more then 5 mb, gif , broken image and txt', () => {

        AddPetPage.uploadPetPhotoButton()
            .click()

        AddPetPage.uploadPhotoFiled()
            .attachFile('imagesAddPet/639.png');

        AddPetPage.emptyPetPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid add-pet-container')

        AddPetPage.validationText()
            .should('be.visible')


        AddPetPage.uploadPhotoFiled()
            .attachFile('imagesAddPet/639.jpeg');

        AddPetPage.emptyPetPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid add-pet-container')

        AddPetPage.validationText()
            .should('be.visible')

        AddPetPage.uploadPhotoFiled()
            .attachFile('imagesAddPet/639.jpeg');

        AddPetPage.emptyPetPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid add-pet-container')

        AddPetPage.validationText()
            .should('be.visible')

        AddPetPage.uploadPhotoFiled()
            .attachFile('/myAccountImages/800px-Snake_River_(5mb).jpg');

        AddPetPage.emptyPetPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid add-pet-container')

        AddPetPage.validationText()
            .should('be.visible')

        AddPetPage.uploadPhotoFiled()
            .attachFile('/myAccountImages/index.txt', { allowEmpty: true });

        AddPetPage.emptyPetPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid add-pet-container')

        AddPetPage.validationText()
            .should('be.visible')

        AddPetPage.uploadPhotoFiled()
            .attachFile('/myAccountImages/PallasJupiter.gif');

        AddPetPage.emptyPetPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid add-pet-container')

        AddPetPage.validationText()
            .should('be.visible')

        AddPetPage.uploadPhotoFiled()
            .attachFile('/myAccountImages/doge.jpg');

        AddPetPage.emptyPetPhotoPopup()
            .should('have.class', 'empty-photo-popup inValid add-pet-container')

        AddPetPage.validationText()
            .should('be.visible')
    })

    it('Positive: Uploading correct jpeg image', () => {

        AddPetPage.stateField()
            .click()

        cy.get('#downshift-0-item-1', { timeout: 30000 })
            .click()

        AddPetPage.petNameField()
            .as('petName')
            .clear()

        cy.fillField(5, '@petName')

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="dog"]')
            .click()

        AddPetPage.contestPetBlock()
            .should('be.visible')
            .contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        cy.get('#downshift-1-item-1', { timeout: 30000 })

        AddPetPage.uploadPetPhotoButton()
            .click()

        AddPetPage.uploadPhotoFiled()
            .attachFile('/imagesAddPet/640.jpeg');

        AddPetPage.changeButton()
            .should('be.visible')

        AddPetPage.imageCrop()
            .should('be.visible')

        AddPetPage.applyPhotoButton()
            .should('be.visible')
            .click()

        AddPetPage.contestPetBlock()
            .find('[type=checkbox]')
            .click()

        AddPetPage.saveButton()
            .should('be.enabled')
            .click()

        AddPetPage.succesSnackbar()
            .should('be.visible')

    })

    it('Positive: Uploading correct jpg image', () => {

        AddPetPage.stateField()
            .click()

        cy.get('#downshift-0-item-1', { timeout: 30000 })
            .click()

        AddPetPage.petNameField()
            .as('petName')
            .clear()

        cy.fillField(5, '@petName')

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="dog"]')
            .click()

        AddPetPage.contestPetBlock()
            .should('be.visible')
            .contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        cy.get('#downshift-1-item-1', { timeout: 30000 })

        AddPetPage.uploadPetPhotoButton()
            .click()

        AddPetPage.uploadPhotoFiled()
            .attachFile('/imagesAddPet/640.jpg');

        AddPetPage.changeButton()
            .should('be.visible')

        AddPetPage.imageCrop()
            .should('be.visible')

        AddPetPage.applyPhotoButton()
            .should('be.visible')
            .click()

        AddPetPage.contestPetBlock()
            .find('[type=checkbox]')
            .click()

        AddPetPage.saveButton()
            .should('be.enabled')
            .click()

        AddPetPage.succesSnackbar()
            .should('be.visible')


    })

    it('Positive: Uploading correct png image', () => {

        AddPetPage.stateField()
            .click()

        cy.get('#downshift-0-item-1', { timeout: 30000 })
            .click()

        AddPetPage.petNameField()
            .as('petName')
            .clear()

        cy.fillField(5, '@petName')

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="dog"]')
            .click()

        AddPetPage.contestPetBlock()
            .should('be.visible')
            .contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        cy.get('#downshift-1-item-1', { timeout: 30000 })

        AddPetPage.uploadPetPhotoButton()
            .click()

        AddPetPage.uploadPhotoFiled()
            .attachFile('/imagesAddPet/640.png');

        AddPetPage.changeButton()
            .should('be.visible')

        AddPetPage.imageCrop()
            .should('be.visible')

        AddPetPage.applyPhotoButton()
            .should('be.visible')
            .click()

        AddPetPage.contestPetBlock()
            .find('[type=checkbox]')
            .click()

        AddPetPage.saveButton()
            .should('be.enabled')
            .click()

        AddPetPage.succesSnackbar()
            .should('be.visible')

    })
})