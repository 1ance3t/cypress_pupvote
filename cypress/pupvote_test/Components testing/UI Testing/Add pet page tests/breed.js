/// <reference types="Cypress" />
import AddPetPage from '../../../../page_object/add_pet.js'


describe('Breed field', () => {

    beforeEach(() => {

        cy.login();

        AddPetPage.openAddPetPage();

        AddPetPage.сloseModalFb();
    })

    it('Positive: Try to select all dog breeds from list', () => {

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="dog"]')
            .click()

        cy.get('.jss291', { timeout: 30000 })
            .should('be.visible').contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        AddPetPage.listOfItems()
            .find('li').then((li) => {
                for (let i = 0; i < li.length; i++) {
                    cy.get(`#downshift-1-item-${i}`, { timeout: 30000 })
                        .click()

                    cy.get('[id="select-breed"]')
                        .should('have.text', li[i].outerText)

                    cy.get('#downshift-1-input')
                        .click({ force: true })
                        .clear()
                }
            })
    })

    it('Positive: Try to select all cat breeds from list', () => {

        AddPetPage.petTypeSelect()
            .click();

        cy.get('[data-value="cat"]')
            .click()

        cy.get('.jss291', { timeout: 30000 })
            .should('be.visible').contains('Participate in the contest')

        AddPetPage.breedSelect()
            .click()

        AddPetPage.listOfItems()
            .find('li').then((li) => {
                for (let i = 0; i < li.length; i++) {
                    cy.get(`#downshift-1-item-${i}`, { timeout: 30000 })
                        .click()

                    cy.get('[id="select-breed"]')
                        .should('have.text', li[i].outerText)

                    cy.get('#downshift-1-input')
                        .click({ force: true })
                        .clear()
                }
            })
    })


})