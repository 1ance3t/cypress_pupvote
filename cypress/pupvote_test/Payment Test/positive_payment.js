/// <reference types="Cypress" />
import StripePopUp from '../../page_object/stripe_list.js'

context('Testing the positive payment', () => {

    describe('Navigate to pet page and opening stripe pop-up', () => {

        beforeEach(() => {
            cy.login()
            StripePopUp.openPetPage()
            StripePopUp.сloseModal()
            StripePopUp.openPopUp()
            StripePopUp.openStripe()
        })

        it('Positive: Payment test Visa', () => {
            StripePopUp.enteringCard('4242 4242 4242 4242')
            StripePopUp.checkingStaus()

        })

        it('Positive: Payment test Visa (debit)', () => {
            StripePopUp.enteringCard('4000 0566 5566 5556')
            StripePopUp.checkingStaus()

        })

        it('Positive: Payment test Mastercard', () => {
            StripePopUp.enteringCard('5555 5555 5555 4444')
            StripePopUp.checkingStaus()

        })
        it('Positive: Payment test Mastercard (2-series)', () => {
            StripePopUp.enteringCard('2223 0031 2200 3222')
            StripePopUp.checkingStaus()

        })
        it('Positive: Payment test Mastercard (2-series)', () => {
            StripePopUp.enteringCard('5200 8282 8282 8210')
            StripePopUp.checkingStaus()

        })
        it('Positive: Payment test American Express', () => {
            StripePopUp.enteringCard('3782 822463 10005')
            StripePopUp.checkingStaus()

        })

        it('Positive: Payment test American Express', () => {
            StripePopUp.enteringCard('3714 496353 98431')
            StripePopUp.checkingStaus()

        })

        it('Positive: Payment test Discover', () => {
            StripePopUp.enteringCard('6011 0009 9013 9424')
            StripePopUp.checkingStaus()

        })

        it('Positive: Payment test Discover', () => {
            StripePopUp.enteringCard('6011 1111 1111 1117')
            StripePopUp.checkingStaus()

        })

        it('Positive: Payment test Diners Club (14 digit card)', () => {
            StripePopUp.enteringCard('3622 720627 1667')
            StripePopUp.checkingStaus()

        })
        it('Positive: Payment test JCB)', () => {
            StripePopUp.enteringCard('3566 0020 2036 0505')
            StripePopUp.checkingStaus()

        })

        it('Positive: Payment test UnionPay)', () => {
            StripePopUp.enteringCard('6200 0000 0000 0006')
            StripePopUp.checkingStaus()

        })

    })
})


