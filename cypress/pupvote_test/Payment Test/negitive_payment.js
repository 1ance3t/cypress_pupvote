/// <reference types="Cypress" />
import StripePopUp from '../../../../page_object/stripe_list.js'


context('Testing the negative payment', () => {

    describe('Navigate to pet page and opening stripe pop-up', () => {

        beforeEach(() => {
            cy.login()
            StripePopUp.openPetPage()
            StripePopUp.сloseModal()
            StripePopUp.openPopUp()
            StripePopUp.openStripe()
        })
        
        it('Negative: Results in a charge with a risk_level of highest', () => {
            StripePopUp.enteringCard('4000 0000 0000 4954');
            StripePopUp.checkFailOfPayment();

        })


        it("Negative: Results in a charge with a risk_level of highest. The charge is blocked as it's considered fraudulent", () => {
            StripePopUp.enteringCard('4100 0000 0000 0019');
            StripePopUp.checkFailOfPayment();

        })

        it("Negative: Charge is declined with a card_declined code", () => {
            StripePopUp.enteringCard('4000 0000 0000 0002');
            StripePopUp.checkFailOfPayment();

        })

        it("Negative: Charge is declined with a card_declined code. The decline_code attribute is insufficient_funds", () => {
            StripePopUp.enteringCard('4000 0000 0000 9995');
            StripePopUp.checkFailOfPayment();

        })

        it("Negative: Charge is declined with a card_declined code. The decline_code attribute is lost_card", () => {
            StripePopUp.enteringCard('4000 0000 0000 9987');
            StripePopUp.checkFailOfPayment();

        })
        it("Negative: Charge is declined with a card_declined code. The decline_code attribute is stolen_card", () => {
            StripePopUp.enteringCard('4000 0000 0000 9979');
            StripePopUp.checkFailOfPayment();

        })
        it("Negative: Charge is declined with an expired_card code", () => {
            StripePopUp.enteringCard('4000 0000 0000 0069');
            StripePopUp.checkFailOfPayment();

        })
        it("Negative: Charge is declined with an incorrect_cvc code", () => {
            StripePopUp.enteringCard('4000 0000 0000 0127');
            StripePopUp.checkFailOfPayment();

        })
        it("Negative: Charge is declined with a processing_error code", () => {
            StripePopUp.enteringCard('4000 0000 0000 0119');
            StripePopUp.checkFailOfPayment();

        })

    })
})