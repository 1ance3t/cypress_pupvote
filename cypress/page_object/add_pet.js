class AddPetPage {
    openAddPetPage() {
        cy.visit(`${Cypress.env('dev')}pet`);
    }

    сloseModalFb() {
        return cy
            .get('.MuiSvgIcon-root-35', { timeout: 6000 })
            .click();
    }


    stateField() {
        return cy
            .get('[id="downshift-0-input"]')
    }

    listOfItems() {
        return cy
            .get('[role="listbox"]')
    }

    petTypeSelect() {
        return cy
            .get('#select-type')
    }

    catType() {
        return cy
            .get('.jss314 > [tabindex="0"]')
    }

    breedSelect() {
        return cy
            .get(':nth-child(7) > .pet_selectWithSearch__1Px7Y > [role="combobox"] > .jss118 > .jss153')
    }

    petNameField() {
        return cy
            .get('#root > div > div:nth-child(2) > div.pet_body__1fq5F.pet-page > div > div.pet_container__33idr > div.pet_petColumn__2mPDm > div > div:nth-child(5) > div > div > input')
    }

    petNameFieldCounter() {
        return cy
            .get('#root > div > div:nth-child(2) > div.pet_body__1fq5F.pet-page > div > div.pet_container__33idr > div.pet_petColumn__2mPDm > div.pet_card__3XKaf.pet_cardInfo__3Ni_a > div:nth-child(5) > div > p > span > span')
    }

    bioFieldCounter() {
        return cy
            .get('#root > div > div:nth-child(2) > div.pet_body__1fq5F.pet-page > div > div.pet_container__33idr > div.pet_petColumn__2mPDm > div.pet_card__3XKaf.pet_cardInfo__3Ni_a > div.pet_inputWrapper__2ZRH4.pet_inputWrapperTwo__3LaEu.pet_bioWrapper__2VKpc > div > p > span > span')
    }

    uploadPhotoFiled() {
        return cy
            .get('[id="upload-image"]')
    }

    applyPhotoButton() {
        return cy
            .get('.btn-group > .primary')
    }

    contestPetBlock() {
        return cy
            .get('.pet_contestPetBlock__2oWGL', { timeout: 30000 })
    }

    bioField() {
        return cy
            .get('#root > div > div:nth-child(2) > div.pet_body__1fq5F.pet-page > div > div.pet_container__33idr > div.pet_petColumn__2mPDm > div.pet_card__3XKaf.pet_cardInfo__3Ni_a > div.pet_inputWrapper__2ZRH4.pet_inputWrapperTwo__3LaEu.pet_bioWrapper__2VKpc > div > div > textarea')
    }

    saveButton() {
        return cy
            .get('#root > div > div:nth-child(2) > div.pet_body__1fq5F.pet-page > div > div.pet_container__33idr > div.pet_actionsRow__3sk-6.pet_createActionsRow__3ihJ9 [type="button"]')
    }

    succesSnackbar(){
        return cy
            .get('#message-id', {timeout: 3000})
    }

    uploadPetPhotoButton(){
        return cy  
            .get('#root > div > div:nth-child(2) > div.pet_body__1fq5F.pet-page > div > div.pet_container__33idr > div.pet_card__3XKaf.pet_cardPicture__3DlZS > div.pet_changeAvatar__nFdUB [type="button"]')
    }

    changePetPhotoButton(){
        return cy
            .get('#root > div > div:nth-child(2) > div.pet_body__1fq5F.pet-page > div > div.pet_container__33idr > div.pet_card__3XKaf.pet_cardPicture__3DlZS > div.pet_changeAvatar__nFdUB > button')
    }

    emptyPetPhotoPopup(){
        return cy
            .get('.empty-photo-popup')
    }

    validationText(){
        return cy
            .get('.validation-text')
    }

    changeButton(){
        return cy
            .get('.btn-group > .outlined')
    }

    imageCrop(){
        return cy
            .get('.image-crop-padding')
    }

    warningAlert(){
        return cy
            .get('.login-modal-container', { timeout: 50000 })
            .should('be.visible')
            .get('.login-modal-container > .primary')
            .click()
    }

}

export default new AddPetPage()