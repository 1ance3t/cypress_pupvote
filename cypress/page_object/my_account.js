class MyAccount {

    openMyAccount() {
        cy.visit(`${Cypress.env('dev')}account`);
    }

    сloseModalFb() {
        return cy
            .get('.MuiSvgIcon-root-35', { timeout: 6000 })
            .click();
    }

    typeName(textName) {
        return cy
            .get('#root > div > div:nth-child(2) > div.myaccount_body__3f2pO.my-account-page > div > div.myaccount_container__2w56L > div.myaccount_card__3Fvuc.myaccount_cardInfo__1YBPP > div.myaccount_inputWrapper__3EEMJ.input-wrapper-first > div > div > input', { timeout: 6000 })
            .click()
            .clear()
            .type(`${textName}`)
            .should('have.value', `${textName}`)
    }

    nameSymbolCounter() {
        return cy
            .get('#root > div > div:nth-child(2) > div.myaccount_body__3f2pO.my-account-page > div > div.myaccount_container__2w56L > div.myaccount_card__3Fvuc.myaccount_cardInfo__1YBPP > div.myaccount_inputWrapper__3EEMJ.input-wrapper-first > div > p > span > span')
    }

    typeCity(textName) {
        return cy
            .get('#root > div > div:nth-child(2) > div.myaccount_body__3f2pO.my-account-page > div > div.myaccount_container__2w56L > div.myaccount_card__3Fvuc.myaccount_cardInfo__1YBPP > div.myaccount_inputWrapper__3EEMJ.myaccount_inputWrapperTwo__2cawm.myaccount_locationWrapper__11vZ3 > div > div > input', { timeout: 6000 })
            .click()
            .clear()
            .type(`${textName}`)
            .should('have.value', `${textName}`)
    }

    uploadPhotoButton() {
        return cy
        .get('#root > div > div:nth-child(2) > div.myaccount_body__3f2pO.my-account-page > div > div.myaccount_container__2w56L > div.myaccount_card__3Fvuc.myaccount_cardPicture__1zpcY > div.myaccount_changeAvatar__dl5Mu > button')
    }

    uploadPhotoField() {
        return cy
        .get('#upload-image')
    }

    emptyPhotoPopup(){
        return cy
        .get('.empty-photo-popup')
    }

    validationText(){
        return cy
            .get('.validation-text')
    }

    
    applyPhotoBtn(){
        return cy
            .get('.btn-group > .primary')
    }

    changeButton(){
        return cy
            .get('.btn-group > .outlined')
    }

    imageCrop(){
        return cy
            .get('.image-crop-padding')
    }

    saveButton() {
        return cy
            .get('.primary', { timeout: 6000 });
    }

    succesSnackbar() {
        return cy
            .get('#message-id', { timeout: 6000 })
            .should('have.text', 'Profile was successfully updated');
    }

    warningAlert() {
        return cy
            .get('.login-modal-container', { timeout: 50000 })
            .should('be.visible')
            .get('.login-modal-container > .primary')
            .click()
    }

    citySymbolCounter() {
        return cy
            .get('#root > div > div:nth-child(2) > div.myaccount_body__3f2pO.my-account-page > div > div.myaccount_container__2w56L > div.myaccount_card__3Fvuc.myaccount_cardInfo__1YBPP > div.myaccount_inputWrapper__3EEMJ.myaccount_inputWrapperTwo__2cawm.myaccount_locationWrapper__11vZ3 > div > p > span > span')
    }

    cityField() {
        return cy
            .get('#root > div > div:nth-child(2) > div.myaccount_body__3f2pO.my-account-page > div > div.myaccount_container__2w56L > div.myaccount_card__3Fvuc.myaccount_cardInfo__1YBPP > div.myaccount_inputWrapper__3EEMJ.myaccount_inputWrapperTwo__2cawm.myaccount_locationWrapper__11vZ3 > div > div > input', { timeout: 6000 })
    }

    nameField() {
        return cy
            .get('#root > div > div:nth-child(2) > div.myaccount_body__3f2pO.my-account-page > div > div.myaccount_container__2w56L > div.myaccount_card__3Fvuc.myaccount_cardInfo__1YBPP > div.myaccount_inputWrapper__3EEMJ.input-wrapper-first > div > div > input', { timeout: 6000 })
    }

    cancelButton() {
        return cy
            .get('.myaccount_actionsRight__1NEvH > .outlined')
    }

    emailCheckboxes(){
        return cy
            .get('#root > div > div:nth-child(2) > div.myaccount_body__3f2pO.my-account-page > div > div.myaccount_container__2w56L > div.myaccount_card__3Fvuc.myaccount_cardInfo__1YBPP > div.myaccount_inputWrapper__3EEMJ.myaccount_inputWrapperTwo__2cawm.myaccount_checkBoxesWrapper__199Xe')
            .find('[type="checkbox"]')
    }

    nameFieldValue(){
        return cy
            .get('.input-wrapper-first > .jss115 > .jss150 > .jss160')
    }

    cityFieldValue(){
        return cy
            .get('.myaccount_locationWrapper__11vZ3 > .jss115 > .jss150 > .jss160')
    }
}

export default new MyAccount()