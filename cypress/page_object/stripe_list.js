class StripePopUp {
    openPetPage() {
        cy.visit(`${Cypress.env('dev')}detail/lola_51`);
    }

    openPopUp() {
        return cy
            .get('#root > div > div:nth-child(2) > div.DetailedView_detailedView__3G9gv > div.pet-info > div > div.pet-description > div.pet-bottom > div > div.petInfo_petInfoBottomRow__2eYJN > div.btn-block.petInfo_voteForPetButton__1W6rp > button')
            .contains('Vote for Pet')
            .click()
    }

    openStripe() {
        return cy
            .contains('+100 votes')
            .click();
    }

    enteringCard(cardnumber) {
        cy.get('.stripe-input', { timeout: 60000 })
            .click();
        cy.get('.__PrivateStripeElement > iframe', { timeout: 60000 }).as('stripe')
            .iframeLoaded()
            .its('document')
            .getInDocument('input[name="cardnumber"]', { timeout: 60000 })
            .type(`${cardnumber}`)
            .should('have.value', `${cardnumber}`);
        cy.get('@stripe')
            .iframeLoaded()
            .its('document')
            .getInDocument('[name="exp-date"]')
            .type('1220')
            .should('have.value', '12 / 20');
        cy.get('@stripe')
            .iframeLoaded()
            .its('document')
            .getInDocument('[name="cvc"]')
            .type('424')
            .should('have.value', '424');
        cy.get('@stripe')
            .iframeLoaded()
            .its('document')
            .getInDocument('[name="postal"]')
            .type('12345')
            .should('have.value', '12345');
    }

    checkFailOfPayment() {
        cy.get('.pet-votes', { timeout: 60000 }).then(($countOfVotes) => {
            const votes = parseInt($countOfVotes.text());
            cy.contains('Pay $1.99 for 100 votes')
                .click();
            cy.get('.finalPaymentTitle', { timeout: 60000 }).then(($finalPaymentResult) => {
                const finalPaymentResu = $finalPaymentResult.text();
                if (finalPaymentResu == 'Payment was failed') {
                    cy.get('#message-id', { timeout: 60000 })
                        .should('be.visible');
                    assert.isOk('Payment was failed');
                    cy.get('.paymentFailed > .jss32 > .jss7', { timeout: 60000 }).then(($textOfSnackbar) => {
                        cy.log($textOfSnackbar.text())
                    });
                    cy.get('.closeBtn-modal')
                        .click()
                    cy.get('.btn-block > .jss32 > .jss7')
                        .click()
                    cy.get('.pet-votes', { timeout: 60000 }).then(($newVotes) => {
                        let newVotes = parseInt($newVotes.text());
                        assert.equal(newVotes, votes)
                    })
                }else{
                    assert.isNotOk('Payment Successful')
                }
            })
        })
    }


    checkingStaus() {
        cy.get('.pet-votes', { timeout: 60000 }).then(($countOfVotes) => {
            const votes = parseInt($countOfVotes.text());
            cy.get('.stripe-button-wrapper > .jss32 > .jss7')
                .click();
            cy.get('.finalPaymentTitle', { timeout: 60000 }).then(($finalPaymentResult) => {
                const finalPaymentResu = $finalPaymentResult.text();
                if (finalPaymentResu == 'Payment Successful') {
                    assert.isOk('Payment Successful')
                    cy.get('#message-id', { timeout: 60000 })
                        .should('be.visible')
                        .and('have.text', 'Thank you for voting! Your vote has been counted and added')
                    cy.get('.paymentFailed > .jss32')
                        .click()
                    cy.get('.btn-block > .jss32 > .jss7')
                        .click()
                    cy.get('.pet-votes', { timeout: 60000 }).then(($newVotes) => {
                        let newVotes = parseInt($newVotes.text());
                        assert.isAbove(newVotes, votes)
                    })
                } else {
                    assert.isNotOk('Payment was failed');
                    cy.get('.jss223', { timeout: 60000 }).then(($textOfSnackbar) => {
                        cy.log($textOfSnackbar.text())
                    });
                    cy.get('.MuiSvgIcon-root-35')
                        .click()
                    cy.get('.btn-block > .jss32 > .jss7')
                        .click()
                    cy.get('.pet-votes', { timeout: 60000 }).then(($newVotes) => {
                        let newVotes = parseInt($newVotes.text());
                        assert.equal(newVotes, votes)
                    })
                }

            })
        })
    }

    сloseModal() {
        return cy
            .get('.MuiSvgIcon-root-35', { timeout: 6000 })
            .click();
    }

}


export default new StripePopUp()